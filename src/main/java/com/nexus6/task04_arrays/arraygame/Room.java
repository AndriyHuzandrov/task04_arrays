package com.nexus6.task04_arrays.arraygame;

import java.util.Random;

public class Room {
  private static  int[] doors;

  public Room() {
    fillDoors();
  }
  private static void fillDoors() {
    int max = 80;
    int min = -100;
    Random rnd = new Random();
    doors = new int[10];
    int behindDoorData ;
    for(int i = 0; i < doors.length; i++) {
      behindDoorData = 0;
      while ((behindDoorData > -5 && behindDoorData < 10)
              || behindDoorData < -100) {
        behindDoorData = rnd.nextInt(181) + min;
        System.out.println(behindDoorData);

      }
       doors[i] = behindDoorData;
    }
  }
  int getFatalDoors() {
    return 0;
  }
  private int[] walkThrough() {
    int[] route;
    int initPower = 25;
    for(int i = 0; i < doors.length; i++) {

    }
    return new int[0];
  }
  private String printRoomMap() {
    String what;
    StringBuffer sb = new StringBuffer();
    sb.append(String.format("%5s %20s%n", "Door #", "Behind the door"));
    for(int i = 0; i < doors.length; i++) {
      what = (doors[i] > 0 ? "artef power " + doors[i] : "monster power " + -(doors[i]));
      sb.append(String.format("%d%30s%n", i, what));
    }
    return sb.toString();
  }

  public static void main(String[] args) {
    Room rm = new Room();
    System.out.println(rm.printRoomMap());
  }
}
