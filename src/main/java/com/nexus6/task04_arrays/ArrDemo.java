package com.nexus6.task04_arrays;

import com.nexus6.task04_arrays.arrops.ArrOps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ArrDemo {
  private static final Logger topLog = LogManager.getLogger(ArrDemo.class);


  public static void main(String[] args) {
    ArrOps ob = new ArrOps(topLog);
    Integer[] ar1 =  {1, 2, 3, 4, 5, 7, 7,90,43};
    Integer[] ar2 =  {7, 8, 9, 56, 5};
    Integer[] ar3 = {1,1, 1, 2, 3, 5, 5, 7, 7, 7, 90,41, 42, 43, 43};

    try {
      for(int n : ob.copyPresentBoth(ar1, ar2)) {
        topLog.info("doubles are " + n);
      }
      for(int n : ob.copyPresentOne(ar1, ar2)) {
        topLog.info("diffs are " + n);
      }
      for(int n : ob.removeTriples(ar3)) {
        topLog.info("without triples " + n);
      }

      for(int n : ob.retainOneInSeq(ar3)) {
        topLog.info("one in seq " + n);
      }

    } catch (IllegalArgumentException e) {
      topLog.error("Wrong arg was passed", e);
    }
  }
}
