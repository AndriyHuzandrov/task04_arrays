package com.nexus6.task04_arrays.arrops;

import java.util.Arrays;
import org.apache.logging.log4j.Logger;

public class ArrOps {

  private static Logger appLog;
  private int[] arrOfDupl;
  private int[] arrFreeOfTriple;

  private class Sequence {
    private int seqStart;
    private int seqEnd;
    private int seqLength;

    private Sequence(int seqStarrt, int seqEnd) {
      this.seqStart = seqStarrt;
      this.seqEnd = seqEnd;
      seqLength = culcSeqLength();
    }

    private int getSeqStart() {
      return seqStart;
    }

    private int getSeqEnd() {
      return seqEnd;
    }

    private int getSeqLength() {
      return seqLength;
    }
    private int culcSeqLength() {
      return seqEnd - seqStart;
    }
  }

  public ArrOps(Logger log) {
    appLog = log;
  }

  public <T extends Integer> int[] copyPresentBoth(T[] firstNums, T[] secondNums)
      throws IllegalArgumentException {
    arrOfDupl = new int[0];
    if (firstNums.length == 0 || secondNums.length == 0) {
      appLog.warn("Can not operate with an empty array");
      throw new ArrayStoreException();
    } else if (firstNums.length >= secondNums.length) {
        appLog.trace("length selected " + firstNums.length);
        gatherDuplicates(firstNums, secondNums);
    } else {
      gatherDuplicates(secondNums, firstNums);
    }
    return arrOfDupl;
  }
  public <T extends Integer> int[] copyPresentOne(T[] firstNums, T[] secondNums)
      throws ArrayStoreException {
    arrOfDupl = new int[0];
    if (firstNums.length == 0 || secondNums.length == 0) {
      appLog.warn("Can not operate with an empty array");
      throw new IllegalArgumentException();
    } else {
      gatherDiff(firstNums, secondNums);
      gatherDiff(secondNums, firstNums);
    }
    return arrOfDupl;
  }

  private <T extends Integer> void gatherDuplicates(T[] patternArr, T[] matchArr) {
    for(int num : patternArr) {
      if(isInArray(num, matchArr)) {
        arrOfDupl = appendToArr(arrOfDupl, num);
      }
    }
  }
  private <T extends Integer> void gatherDiff(T[] patternArr, T[] matchArr) {
    for(int num : patternArr) {
      if(!isInArray(num, matchArr)) {
        arrOfDupl = appendToArr(arrOfDupl, num);
      }
    }
  }

  private <T extends Integer> boolean isInArray(int needle, T[] stack) {
    boolean isIn = false;
    Arrays.sort(stack);
    if (Arrays.binarySearch(stack, needle) >= 0) {
      isIn = true;
    }
    return isIn;
  }

  private int[] appendToArr(int[] numArr, int addNum) {
    int[] extendedArr = new int[numArr.length + 1];
    for(int i = 0; i < extendedArr.length; i++) {
      if(i == numArr.length) {
        extendedArr[i] = addNum;
        break;
      }
      extendedArr[i] = numArr[i];
    }
    return extendedArr;
  }

  public <T extends Integer> int[] removeTriples(T[] inputArr) {
    int[] tempArr = new int[inputArr.length];
    arrFreeOfTriple = new int[0];
    if (inputArr.length == 0) {
      throw new ArrayStoreException();
    } else {
      for(int i = 0; i < inputArr.length; i++) {
        tempArr[i] = inputArr[i];
      }
      for (int i = 0; i < tempArr.length; ) {
        i = (findSeq(tempArr, i).getSeqLength() > 2 ?
            findSeq(tempArr, i).getSeqEnd() : ++i);
        if (i == tempArr.length) {
          break;
        }
        arrFreeOfTriple = appendToArr(arrFreeOfTriple, tempArr[i]);

      }
    }
    return arrFreeOfTriple;
  }
  public <T extends Integer> int[] retainOneInSeq(T[] inputArr) {
    int[] tempArr = new int[inputArr.length];
    arrFreeOfTriple = new int[0];
    if (inputArr.length == 0) {
      appLog.warn("Can not operate with an empty array");
      throw new ArrayStoreException();
    } else {
      for(int i = 0; i < inputArr.length; i++) {
        tempArr[i] = inputArr[i];
      }
      for (int i = 0; i < tempArr.length; ) {
        appLog.trace("i = " + i);
        if(i == findSeq(tempArr, i).getSeqStart()) {
          arrFreeOfTriple = appendToArr(arrFreeOfTriple, tempArr[i]);
          i =  findSeq(tempArr, i).getSeqEnd();
        } else {
          arrFreeOfTriple = appendToArr(arrFreeOfTriple, tempArr[i++]);
        }
      }
    }
    return arrFreeOfTriple;
  }
  private  ArrOps.Sequence findSeq(int[] numArr, int startIndex) {
    int stBound = startIndex;
    int endBound;
    boolean isSeq = false;
    for(endBound = stBound + 1; endBound < numArr.length; stBound++, endBound++) {
      if(numArr[stBound] == numArr[endBound]) {
        isSeq = true;
        while(numArr[endBound] == numArr[stBound]) {
          if(endBound == (numArr.length - 1)) {
            break;
          }
          endBound++;
        }
        break;
      }
    }
    if(!isSeq) {
      stBound = endBound = -1;
    }
    return this.new Sequence(stBound, endBound);
  }
}
